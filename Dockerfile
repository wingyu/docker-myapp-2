FROM ruby:2.3.0

EXPOSE 80 443 3000 9292 5432
#Install NGINX
RUN apt-get update -y -qq && \
  apt-get install -y  vim && \
  apt-get install -y build-essential libpq-dev nodejs && \
  apt-get install curl git-core nginx -y && \
  service nginx start


#Pre-install Nokogiri as it seems to cause problems in BUNDLE INSTALL
RUN gem install nokogiri

#Cache gemfile
WORKDIR /tmp
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install

#Migrate Rails
WORKDIR /myapp
ADD . .
ENV SECRET_KEY_BASE $(bundle exec rake secret)

ENV RAILS_ENV production
RUN RAILS_ENV=production bundle exec rake assets:precompile

RUN service nginx stop

#Turn off when using compose
#CMD bundle exec rake db:migrate; bundle exec puma -C config/puma.rb -d ; nginx -g "daemon off;"
#CMD  rails server --port 3000 --binding 0.0.0.0
CMD bundle exec rake db:migrate;\
  RAILS_ENV=production bundle exec rake assets:precompile;\
  rake search:create_articles_index;\
  bundle exec puma -C config/puma.rb
