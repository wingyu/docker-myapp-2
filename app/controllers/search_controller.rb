class SearchController < ApplicationController
  def search
    if params[:q].nil? || params[:q].blank?
      @articles = Article.all
    else
      @articles = Article.search params[:q]
    end
  end
end
