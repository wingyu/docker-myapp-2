USERNAME= wingyu
PROJECT = docker-myapp
TAG = 1.0
.PHONY: build, run, sidekiq-run

build:
	docker build --rm -t $(USERNAME)/$(PROJECT):$(TAG) .

run:
	docker run -it -d -p 80:80  $(USERNAME)/$(PROJECT):$(TAG)

sidekiq-run:
	docker run -it -d $(USERNAME)/$(PROJECT):$(TAG)	bundle exec sidekiq -L log/sidekiq.log -C config/sidekiq.yml -e production

