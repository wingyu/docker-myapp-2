require 'elasticsearch/rails/tasks/import'

namespace :search do
  desc "description"
  task :create_articles_index => :environment do
    puts "Creating Index for Articles"
    Article.__elasticsearch__.create_index!
    puts "Importing Data"
    Article.import
  end
end

